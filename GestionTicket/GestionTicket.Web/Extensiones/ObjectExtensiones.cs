﻿using System;

namespace System
{
    public static class ObjectExtensiones
    {
        public static bool EsNulo(this object objeto)
        {
            return (objeto == null);
        }

        public static bool EsEnBlanco(this object objeto)
        {
            return (objeto == "");
        }

        public static bool EsFechaValida(this DateTime fecha)
        {
            bool valor=false;
            DateTime FechaInicio=Convert.ToDateTime("01/01/1900");
            DateTime FechaFin=Convert.ToDateTime("31/12/9999");
            if(fecha>FechaInicio && fecha<FechaFin) {valor=true;}
            return valor;
        }

        public static bool EsComboSinSeleccionar(this int valor)
        { 
            bool value = false;
            if (valor == 0) { value = true; }
            return value;
        }
        public static bool EsComboSinSeleccionarString(this string valor)
        {
            bool value = false;
            if (valor == "0") { value = true; }
            return value;
        }
    }
}