﻿using System.Web;
using System.Web.Optimization;

namespace GestionTicket.Web
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //Pagina Errores
            bundles.Add(new StyleBundle("~/bundles/errores")
                .Include("~/Content/errorstyle.css"));

            bundles.Add(new StyleBundle("~/bundles/datepicker")
                .Include("~/Content/datepicker.css",
                         "~/Scripts/bootstrap-datepicker.js"));  

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/Styles.css"));
            bundles.Add(new ScriptBundle("~/bundles/librerias")
                .Include("~/js/librerias.js"));
            bundles.Add(new ScriptBundle("~/bundles/unobtrusive")
                .Include("~/Scripts/jquery.unobtrusive-ajax.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.js")
                                            );
        }
    }
}
