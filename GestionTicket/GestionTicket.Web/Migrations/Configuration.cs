namespace GestionTicket.Web.Migrations
{
    using GestionTicket.Web.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GestionTicket.Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GestionTicket.Web.Models.ApplicationDbContext context)
        {
            var userManager =
                new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager =
                new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // Crear Roles

            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Cliente"));
            roleManager.Create(new IdentityRole("Empleado"));

            // Crear Usuario (admin)
            var user = new ApplicationUser()
            {
                UserName = "admin@ticket.com",
                Email = "admin@ticket.com",
                Area = "Soporte",
                Nombre = "Administrador",
                DebeCambiarPassword=false
            };

            userManager.Create(user, "@d3s4rr0ll0");
            userManager.AddToRole(user.Id, "Admin");

            // Crear Usuario (empleado)
            var empleado = new ApplicationUser()
            {
                UserName = "empleado@ticket.com",
                Email = "empleado@ticket.com",
                Area = "QA",
                Nombre = "Empleado de Prueba",
                DebeCambiarPassword=false

            };

            userManager.Create(empleado, "@d3s4rr0ll0");
            userManager.AddToRole(empleado.Id, "Empleado");


            var cliente = new ApplicationUser()
            {
                UserName = "cliente@ticket.com",
                Email = "cliente@ticket.com",
                Nombre = "Cliente de Prueba",
                DebeCambiarPassword=false
            };

            userManager.Create(cliente, "@d3s4rr0ll0");
            userManager.AddToRole(cliente.Id, "Cliente");
        }
    }
}
