﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestionTicket.Web.Startup))]
namespace GestionTicket.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
