﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult NuevoRequerimiento()
        {
            return RedirectToAction("Nuevo", "Requerimiento", new { area = "Administrativa" });
        }
        public ActionResult ListaRequerimientos()
        {
            return RedirectToAction("RequerimientoListar", "Requerimiento", new { area = "Administrativa" });
        }
        public ActionResult Indicadores()
        {
            return RedirectToAction("Indicadores", "Requerimiento", new { area = "Administrativa" });
        }
    }
}