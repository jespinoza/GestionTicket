﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class ClienteListaViewModel
    {
        public int ClienteId { get; set; }
        public string NombreCliente { get; set; }
    }
}