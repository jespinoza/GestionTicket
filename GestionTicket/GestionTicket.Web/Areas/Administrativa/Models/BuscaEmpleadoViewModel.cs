﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class BuscaEmpleadoViewModel
    {
        public BuscaEmpleadoViewModel() 
        {
            Empleados=new List<EmpleadoListaViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<EmpleadoListaViewModel> Empleados { get; set; }
    }
}