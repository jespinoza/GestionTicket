﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class PuestoViewModel
    {
        [Required(ErrorMessage="Ingrese el nombre del puesto")]
        [DisplayName("Nombre : ")]
        public string NombrePuesto { get; set; }
        [Required(ErrorMessage="Ingrese la descripción del puesto")]
        [DisplayName("Descripción : ")]
        public string DescripcionPuesto { get; set; }
    }
}