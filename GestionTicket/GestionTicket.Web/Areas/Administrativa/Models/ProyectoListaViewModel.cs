﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class ProyectoListaViewModel
    {
        public string ProyectoId { get; set; }
        public string NombreProyecto { get; set; }
    }

    public class TipoListaViewModel
    {
        public string TipoId { get; set; }
        public string NombreTipo { get; set; }
    }

    public class EntornoListaViewModel
    {
        public string EntornoId { get; set; }
        public string NombreEntorno { get; set; }
    }

    public class EstadoListaViewModel
    {
        public string EstadoId { get; set; }
        public string NombreEstado { get; set; }
    }
}