﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class PrioridadListaViewModel
    {
        public string PrioridadId { get; set; }
        public string NombrePrioridad { get; set; }
    }
}