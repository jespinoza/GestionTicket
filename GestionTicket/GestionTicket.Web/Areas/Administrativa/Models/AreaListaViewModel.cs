﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class AreaListaViewModel
    {
        public int AreaId { get; set; }
        public string NombreArea { get; set; }
        public string DescripcionArea { get; set; }
    }
}