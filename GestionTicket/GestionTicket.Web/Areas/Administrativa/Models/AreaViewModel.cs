﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class AreaViewModel
    {
        [DisplayName("Nombre : ")]
        [Required(ErrorMessage="Debe ingresar el nombre del Area")]
        public string NombreArea { get; set; }
        [Required(ErrorMessage="Debe ingresar una descricion del Area")]
        [DisplayName("Descripción : ")]
        public string DescripcionArea { get; set; }
    }
}