﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class EmpleadoListaViewModel
    {
        public int EmpleadoId { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public DateTime FechaInicioLabores { get; set; }
    }
}