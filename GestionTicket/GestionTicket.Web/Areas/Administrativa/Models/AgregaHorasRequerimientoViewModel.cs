﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class AgregaHorasRequerimientoViewModel
    {
        public int RequerimientoId { get; set; }
        public int EmpledoId { get; set; }
        [DisplayName("Fecha de Inicio")]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)] 
            [DataType(DataType.Date)]
        public DateTime FechaInicioReq { get; set; }
        [DisplayName("Horas Trabajadas")]
        public int HorasIngresadas { get; set; }
        [DisplayName("Descripción")]
        public string Descripcion { get; set; }
    }
}