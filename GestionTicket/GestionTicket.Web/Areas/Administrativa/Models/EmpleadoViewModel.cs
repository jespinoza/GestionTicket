﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class EmpleadoGrabarViewModel
    {
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int PuestoId { get; set; }
        public int AreaId { get; set; }
        public string CorreoElectronico { get; set; } 
        [AllowHtml]
        public DateTime FechaInicioLabores { get; set; }
    }

    public class EmpleadoViewModel
    {
        [Required(ErrorMessage="Debe ingresar el Nombre")]
        [DisplayName("Nombre : ")]
        public string Nombres { get; set; }
        [DisplayName("Apellido Paterno : ")]
        [Required(ErrorMessage = "Debe ingresar el Apellido Paterno")]
        public string ApellidoPaterno { get; set; }
        [DisplayName("Apellido Materno : ")]  
        [Required(ErrorMessage = "Debe ingresar el Apellido Materno")]
        public string ApellidoMaterno { get; set; }
        [DisplayName("Puesto : ")] 
        public int PuestoId { get; set; }
        [DisplayName("Area : ")]  
        public int AreaId { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Fecha Inicio : ")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        //[Date]
        public DateTime FechaInicioLabores { get; set; }
        [Required]
        [EmailAddress]
        public string CorreoElectronico { get; set; } 
        [AllowHtml]         
        public SelectList Puestos { get; set; }        
        public SelectList Areas { get; set; }        
    }
}