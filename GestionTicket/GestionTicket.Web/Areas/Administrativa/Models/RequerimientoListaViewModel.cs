﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class RequerimientoListaViewModel
    {
        public int RequerimientoId { get; set; }
        public string NombreRequerimiento { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaDeEntrega { get; set; }
        public DateTime FechaCierreRequerimiento { get; set; }
        public string Prioridad { get; set; }
        public int EmpleadoId { get; set; }
        public string Estado { get; set; }
    }

    public class RequerimientoDetalleListarViewModel
    {
        public int RequeimientoId { get; set; }
        public int EmpleadoId { get; set; }
        public string Empleado { get; set; }
        public int HorasTrabajadas { get; set; }
        public DateTime FechaTrabajada { get; set; }
        public string FechaString { get; set; }
        public string Descripcion { get; set; }
    }
}