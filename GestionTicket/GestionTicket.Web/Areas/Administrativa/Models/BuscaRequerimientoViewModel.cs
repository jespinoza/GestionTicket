﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class BuscaRequerimientoViewModel
    {
        public BuscaRequerimientoViewModel()  
        {
            Requerimientos=new List<RequerimientoListaViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<RequerimientoListaViewModel> Requerimientos { get; set; }
    }

    public class BuscaRequerimientoIndicadoresViewModel
    {
        public BuscaRequerimientoIndicadoresViewModel()
        {
            RequerimientosIndicadores = new List<RequerimientoListaViewModel>();
        }
        public string Busqueda { get; set; }
        public string Estado { get; set; }
        public IEnumerable<RequerimientoListaViewModel> RequerimientosIndicadores { get; set; }
    }
}