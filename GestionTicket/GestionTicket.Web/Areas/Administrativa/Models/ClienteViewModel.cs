﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class ClienteViewModel
    {
        [DisplayName("Nombre : ")]
        [Required(ErrorMessage="Debe ingresar el nombre de la empresa")]
        public string NombreCliente { get; set; }
        [DisplayName("Dirección : ")]
        public string  Direccion { get; set; }
        [Required(ErrorMessage = "Debe ingresar el Ruc")]
        [MaxLength(11)]
        [DisplayName("Ruc : ")]
        public string RucCliente { get; set; }
    }
}