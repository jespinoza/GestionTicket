﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class RequerimientoGrabarViewModel
    {
        public string ProyectoId { get; set; }
        public string Tipo { get; set; }
        public string Resumen { get; set; }
        public int ClienteId { get; set; }
        public int EmpleadoId { get; set; }
        public string Entorno { get; set; }
        public DateTime FechaDeEntrega { get; set; }
        public int HorasEstimadas { get; set; }
        public string Prioridad { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }

    public class RequerimientoEditarViewModel
    {
        public int RequerimientoId { get; set; }
        [DisplayName("Proyecto : ")]
        public string ProyectoId { get; set; }
        public int Proyecto { get; set; }
        [DisplayName("Tipo :")]
        public string Tipo { get; set; }
        [DisplayName("Resumen :")]
        public string Resumen { get; set; }
        [DisplayName("Cliente :")]
        public int ClienteId { get; set; }
        [DisplayName("Cliente :")]
        public string Cliente { get; set; }
        [DisplayName("Encargado : ")]
        public int EmpleadoId { get; set; }
        [DisplayName("Encargado : ")]
        public string Empleado { get; set; }
        [DisplayName("Entorno : ")]
        public string Entorno { get; set; }
        [DisplayName("Fecha Entrega :")]
        public DateTime FechaDeEntrega { get; set; }
        [DisplayName("Fecha de Cierre :")]
        public DateTime FechaCierreRequerimiento { get; set; }
        [DisplayName("Horas Estimadas :")]
        public int HorasEstimadas { get; set; }
        [DisplayName("Prioridad :")]
        public string Prioridad { get; set; }
        [DisplayName("Descripción :")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        [DisplayName("Estado :")]
        public string Estado { get; set; }
        public SelectList Estados { get; set; }
    }
    public class RequerimientoViewModel
    {
        [DisplayName("Proyecto : ")]
        public string ProyectoId { get; set; }
        [DisplayName("Tipo :")]
        [Required]
        public string Tipo { get; set; }
        [DisplayName("Resumen :")]
        [Required]
        public string Resumen { get; set; }
        [DisplayName("Cliente :")]
        [Required]
        public int ClienteId { get; set; }
        [DisplayName("Encargado : ")]
        public int EmpleadoId { get; set; }
        [DisplayName("Entorno : ")]
        public string Entorno { get; set; }        
        [DisplayName("Fecha Entrega :")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime FechaDeEntrega { get; set; }
        [DisplayName("Horas Estimadas :")]
        [Required]
        public int HorasEstimadas { get; set; }
        [DisplayName("Prioridad :")]
        public string Prioridad { get; set; }
        [Required]      
        [DisplayName("Descripcion")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        [AllowHtml]
        public SelectList Proyectos { get; set; }
        public SelectList Clientes { get; set; }
        public SelectList Empleados { get; set; }
        public SelectList Prioridades { get; set; }
        public SelectList Tipos { get; set; }
        public SelectList Entornos { get; set; }
    }

    public class RequerimientoDetalleViewModel
    {
        public int RequeimientoId { get; set; }
        [DisplayName("Empleado : ")]
        public int EmpleadoId { get; set; }
        [DisplayName("Horas : ")]
        public int HorasTrabajadas { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Fecha : ")]
        public DateTime FechaTrabajada { get; set; }
        [DisplayName("Descripción : ")]
        [DataType(DataType.MultilineText)]
        public string Descripcion { get; set; }
        public SelectList Empleados { get; set; }
    }

    public class RequerimientoDetalleGrabarViewModel
    {
        public int RequeimientoId { get; set; } 
        public int EmpleadoId { get; set; }
        public int HorasTrabajadas { get; set; }
        public DateTime FechaTrabajada { get; set; }
        public string Descripcion { get; set; }
    }

    public class EliminarRequerimientoViewModel
    {
        public int RequeimientoId { get; set; }
        public string Descripcion { get; set; }
    }
}