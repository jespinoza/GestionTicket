﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Areas.Administrativa.Models
{
    public class PuestoListaViewModel
    {
        public int PuestoId { get; set; }
        public string NombrePuesto { get; set; }
        public string DescripcionPuesto { get; set; }
    }
}