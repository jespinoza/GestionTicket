﻿using AutoMapper;
using GestionTicket.Servicios.AreaProyecto;
using GestionTicket.Servicios.AreaProyecto.DTOs;
using GestionTicket.Servicios.Puestos;
using GestionTicket.Servicios.Puestos.DTOs;
using GestionTicket.Web.Areas.Administrativa.Models;
using GestionTicket.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionTicket.Servicios.Empleados.DTOs;
using GestionTicket.Servicios.Empleados;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    public class EmpleadoController : Controller
    {
        private ServicioArea servicioArea;
        private ServicioPuesto servicioPuesto;
        private ServicioEmpleado servicioEmpleado;
        private ViewModelBuilder builder;
        // GET: Administrativa/Clientes
        public EmpleadoController()
        {
            servicioArea = new ServicioArea();
            servicioPuesto=new ServicioPuesto();
            servicioEmpleado = new ServicioEmpleado();
            builder = new ViewModelBuilder();
        }
        // GET: Administrativa/Empleado
        public ActionResult IndexEmpleado()
        {
            return View();
        }

        public ActionResult EmpleadoListar()
        {
            return View();
        }

        // GET: Administrativa/Empleado/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administrativa/Empleado/Create
        public ActionResult Nuevo()
        {
            var empleadoViewModel = new EmpleadoViewModel();
            var areas = Mapper.Map<IList<AreaDTO>, IList<AreaListaViewModel>>(servicioArea.BuscarTodos());
            var puestos = Mapper.Map<IList<PuestoDTO>, IList<PuestoListaViewModel>>(servicioPuesto.BuscarTodos());
            empleadoViewModel.Areas = new SelectList(areas, "AreaId", "NombreArea", empleadoViewModel.AreaId);
            empleadoViewModel.Puestos = new SelectList(puestos, "PuestoId", "NombrePuesto", empleadoViewModel.AreaId);
            return View(empleadoViewModel);
            //TODO: traer data Real
            //EmpleadoViewModel empleado = builder.EmpleadoViewModel();

            //return View(empleado);
        }

        // POST: Administrativa/Empleado/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Nuevo(EmpleadoGrabarViewModel empleado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    await CreaUsuario(empleado);
                    GrabaEmpleadoDTO empleadoDTO =
                        Mapper.Map<EmpleadoGrabarViewModel, GrabaEmpleadoDTO>(empleado);
                    if (!empleadoDTO.FechaInicioLabores.EsFechaValida())
                    {
                        ModelState.AddModelError("", "Ingrese una fecha valida");
                        return View(empleado);
                    }
                    servicioEmpleado.Nuevo(empleadoDTO);
                    
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return RedirectToAction("Index", "Administrativa", new { area = "" });
            }
            catch
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View();
            }
        }

        // GET: Administrativa/Empleado/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administrativa/Empleado/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrativa/Empleado/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrativa/Empleado/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult ListaEmpleado(string busqueda)
        {
            try
            {
                busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
                var empleados = Mapper.Map<IList<EmpleadoDTO>, IList<EmpleadoListaViewModel>>(
                    servicioEmpleado.Buscar(busqueda));
                return PartialView("_ListaEmpleado", empleados);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return PartialView();
            }            
        }

        private async Task<IdentityResult> CreaUsuario(EmpleadoGrabarViewModel empleado)
        {
            using (ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>())
            {
                var usuarioEmp = new ApplicationUser()
                {
                    UserName = empleado.CorreoElectronico,
                    Email = empleado.CorreoElectronico,
                    Nombre = empleado.Nombres,
                    DebeCambiarPassword = true
                };

                await userManager.CreateAsync(usuarioEmp, "P@$$w0rd");
                userManager.AddToRole(usuarioEmp.Id, "Empleado");
                string code = await userManager.GeneratePasswordResetTokenAsync(usuarioEmp.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = usuarioEmp.Id, code = code, area = "" }, protocol: Request.Url.Scheme);
                await userManager.SendEmailAsync(usuarioEmp.Id, "Crear contraseña", "Para Crear su contraseña, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");
            }
            return null;
        }
    }
}
