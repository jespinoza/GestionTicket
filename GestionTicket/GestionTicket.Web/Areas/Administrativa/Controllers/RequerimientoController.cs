﻿﻿using AutoMapper;
using GestionTicket.Web.Areas.Administrativa.Models;
using GestionTicket.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionTicket.Servicios.Requerimientos;
using GestionTicket.Servicios.Requerimientos.DTOs;
using GestionTicket.Servicios.Clientes.DTOs;
using GestionTicket.Servicios.Empleados.DTOs;
using GestionTicket.Servicios.Empleados;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    [Authorize(Roles = "Admin, Empleado")]
    public class RequerimientoController : Controller
    {
        private ServicioRequerimiento servicioRequerimiento;
        private ViewModelBuilder builder;
        public RequerimientoController()
        {
            servicioRequerimiento = new ServicioRequerimiento();
            builder = new ViewModelBuilder();
        }
        // GET: Administrativa/Requerimiento
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Editar(int id)
        {
            RequerimientoDTO requerimiento = servicioRequerimiento.TraerPorId(id);
            var estados = new List<EstadoListaViewModel>();
            estados = ObtenerListaEstado();
            if (requerimiento.EsNulo())
                RedirectToAction("NoEncontrado", "Errores", new { area = "" });
            var model = Mapper.Map<RequerimientoDTO, RequerimientoEditarViewModel>(requerimiento);
            model.Estados = new SelectList(estados, "EstadoId", "NombreEstado", model.Estado);
            ClienteDTO cliente = servicioRequerimiento.TraerClientePorId(model.ClienteId);
            EmpleadoDTO empleado = servicioRequerimiento.TraerEmpleadoPorId(model.EmpleadoId);

            model.Cliente = cliente.NombreCliente;
            model.Empleado = (empleado.Nombres + " " + empleado.ApellidoPaterno + " " + empleado.ApellidoMaterno);
            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(RequerimientoEditarViewModel requerimientoEditar)
        {
            try
            {
                // Debemos codificar la reserva
                RequerimientoDTO requerimientoDTO =
                    Mapper.Map<RequerimientoEditarViewModel, RequerimientoDTO>(requerimientoEditar);
                if (requerimientoEditar.Estado == "Listo")
                {
                    requerimientoDTO.FechaCierreRequerimiento = DateTime.Now;
                }
                else
                {
                    requerimientoDTO.FechaCierreRequerimiento = requerimientoDTO.FechaDeEntrega;
                }
                servicioRequerimiento.Actualizar(requerimientoDTO);

                return RedirectToAction("Index", "Home", new { area = "" });
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(requerimientoEditar);
            }
        }
        public ActionResult RequerimientoListar(string busqueda)
        {
            busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
            var model = Mapper.Map<IList<RequerimientoDTO>, IList<RequerimientoListaViewModel>>(servicioRequerimiento.Buscar(busqueda));
            return View(new BuscaRequerimientoViewModel() { Requerimientos = model });
        }

        public ActionResult AgregarHoras(int id, int empleadoId, string Titulo)
        {
            Session["Titulo"] = Titulo;
            return PartialView("_AgregarHoras", new AgregaHorasRequerimientoViewModel() { RequerimientoId = id, EmpledoId = empleadoId });
        }

        public ActionResult EliminarRequerimiento(int id, string Descripcion)
        {     
            return PartialView("_EliminarRequerimiento", new EliminarRequerimientoViewModel() { RequeimientoId = id, Descripcion = Descripcion });
        }
        [HttpPost]
        public ActionResult EliminarRequerimiento(int id)
        {
            bool valor = true;
            valor = servicioRequerimiento.EliminarRequerimientoDetalle(id);
            valor = servicioRequerimiento.EliminarRequerimiento(id);
            return JavaScript("document.location.replace('" + Url.Action("RequerimientoListar", "Requerimiento", new { area="Administrativa" }) + "');");
        }


        // GET: Administrativa/Requerimiento/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult GrabarHoras(AgregaHorasRequerimientoViewModel grabarHorasRequerimiento)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    //// Debemos codificar la reserva
                    //HorasRequerimientoDTO horas =
                    //    Mapper.Map<AgregaHorasRequerimientoViewModel, HorasRequerimientoDTO>(grabarHorasRequerimiento);
                    //servicioRequerimiento.AgregarHoras(horas);
                    //return RedirectToAction("Index", "Home", new { area = "" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(new RequerimientoViewModel());
            }
            catch
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View(new RequerimientoViewModel());
            }
        }

        // GET: Administrativa/Requerimiento/Create
        public ActionResult Nuevo()
        {
            var requerimientoModel = new RequerimientoViewModel();
            requerimientoModel = builder.RequerimientoViewModel();
            return View(requerimientoModel);
        }

        // POST: Administrativa/Requerimiento/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(RequerimientoGrabarViewModel requerimiento)
        {
            try
            {
                if (requerimiento.FechaDeEntrega == DateTime.MinValue)
                {
                    ModelState.AddModelError("", "Debe seleccionar una Fecha");
                    return View(builder.RequerimientoViewModel(requerimiento));
                }

                if (ModelState.IsValid)
                {
                    //validaciones
                    string valor = ValidarNuevoRequerimiento(requerimiento);
                    if (valor != "")
                    {
                        ModelState.AddModelError("", valor);
                        return View(builder.RequerimientoViewModel(requerimiento));
                    }
                    // Debemos codificar la reserva
                    RequerimientoDTO requerimientoDTO =
                        Mapper.Map<RequerimientoGrabarViewModel, RequerimientoDTO>(requerimiento);
                    requerimientoDTO.Estado = "Registrado";
                    requerimientoDTO.FechaCierreRequerimiento = requerimientoDTO.FechaDeEntrega;
                    servicioRequerimiento.Nuevo(requerimientoDTO);
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(builder.RequerimientoViewModel(requerimiento));
            }
            catch
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View(builder.RequerimientoViewModel(requerimiento));
            }
        }

        // GET: Administrativa/Requerimiento/Edit/5
        public ActionResult Detalle(int id)
        {
            RequerimientoDTO requerimiento = servicioRequerimiento.TraerPorId(id);
            var model = Mapper.Map<RequerimientoDTO, RequerimientoEditarViewModel>(requerimiento);
            ClienteDTO cliente = servicioRequerimiento.TraerClientePorId(model.ClienteId);
            EmpleadoDTO empleado = servicioRequerimiento.TraerEmpleadoPorId(model.EmpleadoId);

            model.Cliente = cliente.NombreCliente;
            model.Empleado = (empleado.Nombres + " " + empleado.ApellidoPaterno + " " + empleado.ApellidoMaterno);
            return View(model);
        }

        // POST: Administrativa/Requerimiento/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrativa/Requerimiento/Delete/5
        public ActionResult Indicadores()
        {
            int EstadoRegistrado = 0;
            int EstadoEnDesarrollo = 0;
            int EstadoEnRevicion = 0;
            int EstadoListo = 0;
            int Contador = 0;
            IList<RequerimientoListaViewModel> requerimientos;
            requerimientos = Mapper.Map<IList<RequerimientoDTO>, IList<RequerimientoListaViewModel>>(
                   servicioRequerimiento.BuscarTodos());
            foreach (var item in requerimientos)
            {
                switch (item.Estado)
                {
                    case "Registrado":
                        EstadoRegistrado=EstadoRegistrado+1;
                        break;
                    case "En Desarrollo":
                        EstadoEnDesarrollo = EstadoEnDesarrollo + 1;
                        break;
                    case "En Revisión":
                        EstadoEnRevicion = EstadoEnRevicion + 1;
                        break;
                    case "Listo":
                        EstadoListo = EstadoListo + 1;
                        break;
                }
                Contador = Contador + 1;
            }

            @ViewBag.Registrado = EstadoRegistrado;
            @ViewBag.Desarrollo = EstadoEnDesarrollo;
            @ViewBag.Revision = EstadoEnRevicion;
            @ViewBag.Listo = EstadoListo;
            @ViewBag.PorcRegistrado = Math.Round(((decimal)EstadoRegistrado / (decimal)(Contador)) * 100,2).ToString().Replace(',','.') + "%"; ;
            @ViewBag.PorcDesarrollo = Math.Round(((decimal)EstadoEnDesarrollo / (decimal)Contador) * 100, 2).ToString().Replace(',', '.') + "%";
            @ViewBag.PorcRevision = Math.Round(((decimal)EstadoEnRevicion / (decimal)Contador) * 100, 2).ToString().Replace(',', '.') + "%";
            @ViewBag.PorcListo = Math.Round(((decimal)EstadoListo / (decimal)Contador) * 100, 2).ToString().Replace(',', '.') + "%";
            @ViewBag.TotalReque = Contador;
            return View();
        }

        // POST: Administrativa/Requerimiento/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public PartialViewResult AgregarHorasRequerimiento(int idRequerimiento)
        {
            var requeDetalle = new RequerimientoDetalleViewModel();
            var Correo = Session["CorreoUsuario"].ToString();
            EmpleadoDTO empleado = servicioRequerimiento.TraerEmpleadoPorCorreo(Correo);
            if (empleado != null)
            {
                requeDetalle.EmpleadoId = empleado.EmpleadoId;
            }
            else
            {
                var servicioEmpleado = new ServicioEmpleado();
                //empleados
                var EmpleadoListaViewModel = new EmpleadoListaViewModel();
                EmpleadoListaViewModel.EmpleadoId = 0;
                EmpleadoListaViewModel.Nombres = "--Seleccione--";

                var empleados = Mapper.Map<IList<EmpleadoDTO>, IList<EmpleadoListaViewModel>>(servicioEmpleado.BuscarTodos());
                empleados.Add(EmpleadoListaViewModel);
                requeDetalle.Empleados = new SelectList(empleados.OrderBy(x => x.EmpleadoId), "EmpleadoId", "Nombres", requeDetalle.EmpleadoId);
            }
            requeDetalle.RequeimientoId = idRequerimiento;
            return PartialView("_AgregarHorasRequerimiento", requeDetalle);
        }

        [HttpPost]
        public ActionResult AgregarHorasRequerimiento(RequerimientoDetalleGrabarViewModel requerimientoDetalleGrabarViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar el detalle de la reserva
                    RequerimientoDetalleDTO requerimientoDetalleDTO =
                        Mapper.Map<RequerimientoDetalleGrabarViewModel, RequerimientoDetalleDTO>(requerimientoDetalleGrabarViewModel);
                    servicioRequerimiento.AgregarHorasDetalle(requerimientoDetalleDTO);
                    return JavaScript("document.location.replace('" + Url.Action("Editar", "Requerimiento", new { id = requerimientoDetalleGrabarViewModel.RequeimientoId }) + "');");
                    //return RedirectToAction("Index", "Home", new { area = "" });
                    //return RedirectToAction("Editar", "Requerimiento", new { id=requerimientoDetalleGrabarViewModel.RequeimientoId });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home", new { area = "" });
                throw;
            }
            return View();
        }

        public PartialViewResult ListaRequerimiento(string busqueda)
        {
            try
            {
                busqueda = string.IsNullOrEmpty(busqueda) ? string.Empty : busqueda;
                //var requerimientos = new Object();
                IList<RequerimientoListaViewModel> requerimientos;
                if (busqueda.EsEnBlanco())
                {
                   requerimientos = Mapper.Map<IList<RequerimientoDTO>, IList<RequerimientoListaViewModel>>(
                   servicioRequerimiento.BuscarTodos());
                }
                else
                {
                   requerimientos = Mapper.Map<IList<RequerimientoDTO>, IList<RequerimientoListaViewModel>>(
                   servicioRequerimiento.Buscar(busqueda));
                }
                return PartialView("_ListaRequerimiento", requerimientos);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return PartialView();
            }
        }

        public PartialViewResult VerHistoricoRequerimiento(int id)
        {
            try
            {
            int HorasTotales = 0;
            IList<RequerimientoDetalleListarViewModel> requerimientos;
            requerimientos = Mapper.Map<IList<RequerimientoDetalleDTO>, IList<RequerimientoDetalleListarViewModel>>(
                   servicioRequerimiento.BuscarDetalle(id));
            foreach (var item in requerimientos)
            {
                EmpleadoDTO empleado = servicioRequerimiento.TraerEmpleadoPorId(item.EmpleadoId);
                item.Empleado = empleado.Nombres + " " + empleado.ApellidoPaterno + " " + empleado.ApellidoMaterno;
                item.FechaString = item.FechaTrabajada.Day + "/" + item.FechaTrabajada.Month + "/" + item.FechaTrabajada.Year;
                HorasTotales += item.HorasTrabajadas;
            }
            @ViewBag.HorasTotales = HorasTotales;
            return PartialView("_VerHistoricoRequerimiento",requerimientos);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #region Otros

        public static List<EstadoListaViewModel> ObtenerListaEstado()
        {
            var estados = new List<EstadoListaViewModel>()
            {
                
                new EstadoListaViewModel {EstadoId = "Registrado", NombreEstado = "Registrado"},
                new EstadoListaViewModel {EstadoId = "En Desarrollo", NombreEstado = "En Desarrollo"},
                new EstadoListaViewModel {EstadoId = "En Revisión", NombreEstado = "En Revisión"},
                new EstadoListaViewModel {EstadoId = "Listo", NombreEstado = "Listo"},
            };
            return estados;
        }

        public string ValidarNuevoRequerimiento(RequerimientoGrabarViewModel requerimiento)
        {
            string valor = "";
            if (requerimiento.ProyectoId.EsComboSinSeleccionarString())
            {
                valor= "Debe seleccionar un Proyecto";
            }
            if (requerimiento.Tipo.EsComboSinSeleccionarString())
            {
                valor = "Debe seleccionar un Tipo";
            }
            if (requerimiento.ClienteId.EsComboSinSeleccionar())
            {
                valor = "Debe seleccionar un Cliente";
            }
            if (requerimiento.EmpleadoId.EsComboSinSeleccionar())
            {
                valor = "Debe seleccionar un Empleado";
            }
            if (requerimiento.Entorno.EsComboSinSeleccionarString())
            {
                valor = "Debe seleccionar un entorno de trabajo";
            }
            if (requerimiento.Prioridad.EsComboSinSeleccionarString())
            {
                valor = "Debe seleccionar una prioridad";
            }
            return valor;
        }
        #endregion
    }
}