﻿using AutoMapper;
using GestionTicket.Servicios.Clientes.DTOs;
using GestionTicket.Web.Areas.Administrativa.Models;
using System;
using System.Collections.Generic;
using GestionTicket.Servicios.Clientes;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ClienteController : Controller
    {
        private ServicioCliente servicioClientes;
        // GET: Administrativa/Clientes
        public ClienteController()
        {
            servicioClientes = new ServicioCliente();
        }
        // GET: Administrativa/Cliente
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administrativa/Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administrativa/Cliente/Create
        public ActionResult Nuevo()
        {
            return View(new ClienteViewModel());
        }

        // POST: Administrativa/Cliente/Create
        [HttpPost]
        public ActionResult Nuevo(ClienteViewModel cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    ClienteDTO clietneDto =
                        Mapper.Map<ClienteViewModel, ClienteDTO>(cliente);
                    servicioClientes.Nuevo(clietneDto);
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(new ClienteViewModel());
            }
            catch 
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View(new ClienteViewModel());
            }
        }

        // GET: Administrativa/Cliente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administrativa/Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrativa/Cliente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrativa/Cliente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
