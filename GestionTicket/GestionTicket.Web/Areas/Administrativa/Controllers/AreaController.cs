﻿using GestionTicket.Web.Areas.Administrativa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionTicket.Servicios.AreaProyecto.DTOs;
using GestionTicket.Servicios.AreaProyecto;
using AutoMapper;
using GestionTicket.Servicios.Puestos.DTOs;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AreaController : Controller
    {
        private ServicioArea servicioArea;
        // GET: Administrativa/Clientes
        public AreaController()
        {
            servicioArea = new ServicioArea();
        }

        // GET: Administrativa/Area
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administrativa/Area/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administrativa/Area/Create
        public ActionResult Nuevo()
        {
            return View(new AreaViewModel());
        }

        // POST: Administrativa/Area/Create
        [HttpPost]
        public ActionResult Nuevo(AreaViewModel area)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    AreaDTO areaDTO =
                        Mapper.Map<AreaViewModel, AreaDTO>(area);
                    servicioArea.Nuevo(areaDTO);
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(new ClienteViewModel());
            }
            catch
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View(new AreaViewModel());
            }
        }

        // GET: Administrativa/Area/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administrativa/Area/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrativa/Area/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrativa/Area/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
