﻿using AutoMapper;
using GestionTicket.Web.Areas.Administrativa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionTicket.Servicios.Puestos.DTOs;
using GestionTicket.Servicios.Puestos;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PuestoController : Controller
    {
        private ServicioPuesto servicioPuesto;
        // GET: Administrativa/Clientes
        public PuestoController()
        {
            servicioPuesto = new ServicioPuesto();
        }
        // GET: Administrativa/Puesto
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administrativa/Puesto/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administrativa/Puesto/Create
        public ActionResult Nuevo()
        {
            return View(new PuestoViewModel());
        }

        // POST: Administrativa/Puesto/Create
        [HttpPost]
        public ActionResult Nuevo(PuestoViewModel puesto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Debemos codificar la reserva
                    PuestoDTO puestoDTO =
                        Mapper.Map<PuestoViewModel, PuestoDTO>(puesto);
                    servicioPuesto.Nuevo(puestoDTO);
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                // reconstruir el objeto anterior <ReservaViewModel>
                ModelState.AddModelError("", "Hubo Error en el Modelo");
                return View(new PuestoViewModel());
            }
            catch
            {
                ModelState.AddModelError("", "No se pudieron guardar los datos");
                return View(new PuestoViewModel());
            }
        }

        // GET: Administrativa/Puesto/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administrativa/Puesto/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrativa/Puesto/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrativa/Puesto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
