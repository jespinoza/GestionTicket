﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Areas.Administrativa.Controllers
{
    [Authorize(Roles = "Admin,Empleado")]
    public class AddHomeController : Controller
    {        
        // GET: Administrativa/AddHome
        public ActionResult Index()
        {
            return View();
        }
    }
}