﻿using AutoMapper;
using GestionTicket.Servicios.Clientes;
using GestionTicket.Servicios.Clientes.DTOs;
using GestionTicket.Servicios.Empleados;
using GestionTicket.Servicios.Empleados.DTOs;
using GestionTicket.Servicios.Requerimientos;
using GestionTicket.Web.Areas.Administrativa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTicket.Web.Models
{
    internal class ViewModelBuilder
    {

        //Requerimientos
        #region RegionRequerimientos
        internal RequerimientoViewModel RequerimientoViewModel(RequerimientoGrabarViewModel requerimiento)
        {
            var requerimientoViewModels = new RequerimientoViewModel();
            requerimientoViewModels.ClienteId = requerimiento.ClienteId;
            requerimientoViewModels.Descripcion = requerimiento.Descripcion;
            requerimientoViewModels.EmpleadoId = requerimiento.EmpleadoId;
            requerimientoViewModels.Entorno = requerimiento.Entorno;
            requerimientoViewModels.FechaDeEntrega = requerimiento.FechaDeEntrega;
            requerimientoViewModels.HorasEstimadas = requerimiento.HorasEstimadas;
            requerimientoViewModels.Prioridad = requerimiento.Prioridad;
            requerimientoViewModels.ProyectoId = requerimiento.ProyectoId;
            requerimientoViewModels.Resumen = requerimiento.Resumen;
            requerimientoViewModels.Tipo = requerimiento.Tipo;
            ConfigurarListas(requerimientoViewModels);

            return requerimientoViewModels;
        }

        internal RequerimientoViewModel RequerimientoViewModel()
        {
            var requerimiento = new RequerimientoViewModel();
            ConfigurarListas(requerimiento);
            return requerimiento;
        }


        private static void ConfigurarListas(RequerimientoViewModel requerimientoViewModel)
        {
            var servicioEmpleado = new ServicioEmpleado();
            var servicioCliente = new ServicioCliente();

            //proyectos
            var proyectos = new List<ProyectoListaViewModel>()
            {
                new ProyectoListaViewModel {ProyectoId = "0", NombreProyecto = "--Seleccione--"},
                new ProyectoListaViewModel {ProyectoId = "Desarrollo", NombreProyecto = "Desarrollo"},
                new ProyectoListaViewModel {ProyectoId = "Proyectos", NombreProyecto = "Proyectos"},
                new ProyectoListaViewModel {ProyectoId = "Analisis", NombreProyecto = "Analisis"},
                new ProyectoListaViewModel {ProyectoId = "QA", NombreProyecto = "QA"}
            };

            //prioridades
            var prioridades = new List<PrioridadListaViewModel>()
            {
                new PrioridadListaViewModel {PrioridadId = "0", NombrePrioridad = "--Seleccione--"},
                new PrioridadListaViewModel {PrioridadId = "Baja", NombrePrioridad = "Baja"},
                new PrioridadListaViewModel {PrioridadId = "Media", NombrePrioridad = "Media"},
                new PrioridadListaViewModel {PrioridadId = "Importante", NombrePrioridad = "Importante"},
                new PrioridadListaViewModel {PrioridadId = "Critica", NombrePrioridad = "Critica"}
            };

            //tipos
            var tipos = new List<TipoListaViewModel>()
            {
                new TipoListaViewModel {TipoId = "0", NombreTipo = "--Seleccione--"},
                new TipoListaViewModel {TipoId = "Tarea", NombreTipo = "Tarea"},
                new TipoListaViewModel {TipoId = "Apoyo puntual", NombreTipo = "Apoyo puntual"},
                new TipoListaViewModel {TipoId = "Error en la aplicacion", NombreTipo = "Error en la aplicacion"},
            };

            //entornos
            var entornos = new List<EntornoListaViewModel>()
            {
                new EntornoListaViewModel {EntornoId = "0", NombreEntorno = "--Seleccione--"},
                new EntornoListaViewModel {EntornoId = "Aplicacion", NombreEntorno = "Aplicacion"},
                new EntornoListaViewModel {EntornoId = "Bd", NombreEntorno = "Bd"},
                new EntornoListaViewModel {EntornoId = "Servidor", NombreEntorno = "Servidor"},
                new EntornoListaViewModel {EntornoId = "Otros", NombreEntorno = "Otros"},
            };
            //empleados
            var empleado = new EmpleadoListaViewModel();
            empleado.EmpleadoId = 0;
            empleado.Nombres = "--Seleccione--";

            var empleados = Mapper.Map<IList<EmpleadoDTO>, IList<EmpleadoListaViewModel>>(servicioEmpleado.BuscarTodos());
            empleados.Add(empleado);

            //clientes
            var cliente = new ClienteListaViewModel();
            cliente.ClienteId = 0;
            cliente.NombreCliente = "--Seleccione--";
            var clientes = Mapper.Map<IList<ClienteDTO>, IList<ClienteListaViewModel>>(servicioCliente.BuscarTodos());
            clientes.Add(cliente);

            requerimientoViewModel.Empleados = new SelectList(empleados.OrderBy(x => x.EmpleadoId), "EmpleadoId", "Nombres", requerimientoViewModel.EmpleadoId);
            requerimientoViewModel.Clientes = new SelectList(clientes.OrderBy(x => x.ClienteId), "ClienteId", "NombreCliente", requerimientoViewModel.ClienteId);
            requerimientoViewModel.Proyectos = new SelectList(proyectos, "ProyectoId", "NombreProyecto", requerimientoViewModel.ProyectoId);
            requerimientoViewModel.Prioridades = new SelectList(prioridades, "PrioridadId", "NombrePrioridad", requerimientoViewModel.Prioridad);
            requerimientoViewModel.Tipos = new SelectList(tipos, "TipoId", "NombreTipo", requerimientoViewModel.Tipo);
            requerimientoViewModel.Entornos = new SelectList(entornos, "EntornoId", "NombreEntorno", requerimientoViewModel.Entorno);

        }
        #endregion

    }
}