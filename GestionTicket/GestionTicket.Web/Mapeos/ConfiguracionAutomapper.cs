﻿using AutoMapper;
using GestionTicket.Servicios.AreaProyecto.DTOs;
using GestionTicket.Servicios.Clientes.DTOs;
using GestionTicket.Servicios.Empleados.DTOs;
using GestionTicket.Servicios.Puestos.DTOs;
using GestionTicket.Servicios.Requerimientos.DTOs;
using GestionTicket.Web.Areas.Administrativa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTicket.Web.Mapeos
{
    public class ConfiguracionAutomapper
    {
        public static void Configuracion()
        {
            Cliente();
            Empleado();
            Area();
            Puesto();
            Requerimiento();
        }
        private static void Cliente()
        {
            Mapper.CreateMap<ClienteViewModel, ClienteDTO>();
            Mapper.CreateMap<ClienteDTO, ClienteListaViewModel>();
        }
        private static void Empleado()
        {
            Mapper.CreateMap<EmpleadoGrabarViewModel, GrabaEmpleadoDTO>()
                .ForMember(dest => dest.NombreUsuario,
                        map => map.MapFrom(orig => orig.CorreoElectronico));

            Mapper.CreateMap<EmpleadoDTO, EmpleadoGrabarViewModel>()
                .ForMember(dest => dest.CorreoElectronico,
                    map => map.MapFrom(orig => orig.NombreUsuario));

            Mapper.CreateMap<EmpleadoDTO, EmpleadoListaViewModel>()
                .ForMember(dest=>dest.Nombres,map=>map.MapFrom(orig=>(orig.Nombres+" "+orig.ApellidoPaterno+" "+orig.ApellidoMaterno)));
        }
        private static void Area()
        {
            Mapper.CreateMap<AreaViewModel, AreaDTO>();
            Mapper.CreateMap<AreaDTO, AreaListaViewModel>();
        }
        private static void Puesto()
        {
            Mapper.CreateMap<PuestoViewModel, PuestoDTO>();
            Mapper.CreateMap<PuestoDTO, PuestoListaViewModel>();
        }
        private static void Requerimiento()
        {
            Mapper.CreateMap<RequerimientoGrabarViewModel, RequerimientoDTO>();
            Mapper.CreateMap<RequerimientoDetalleGrabarViewModel, RequerimientoDetalleDTO>();
            Mapper.CreateMap<RequerimientoEditarViewModel, RequerimientoDTO>()
                .ForSourceMember(dest=>dest.Estados,map=>map.Ignore())
                .ForSourceMember(dest => dest.Empleado, map => map.Ignore())
                .ForSourceMember(dest => dest.Proyecto, map => map.Ignore());
            Mapper.CreateMap<RequerimientoDTO, RequerimientoListaViewModel>()
                .ForMember(dest=>dest.NombreRequerimiento,map=>map.MapFrom(orig=>orig.Resumen))
                .ForMember(dest=>dest.EmpleadoId,map=>map.MapFrom(orig=>orig.EmpleadoId));
            Mapper.CreateMap<RequerimientoDTO, RequerimientoEditarViewModel>()
                .ForMember(dest => dest.Proyecto,
                    map => map.Ignore())
                .ForMember(dest => dest.Empleado,
                    map => map.Ignore())
                .ForMember(dest => dest.Cliente,
                    map => map.Ignore());
            Mapper.CreateMap<RequerimientoDetalleDTO, RequerimientoDetalleListarViewModel>()
                .ForMember(dest => dest.Empleado, map => map.Ignore())
                .ForMember(dest => dest.RequeimientoId, map => map.MapFrom(orig => orig.RequeimientoId))
                .ForMember(dest => dest.Descripcion, map => map.MapFrom(orig => orig.Descripcion));
            //Mapper.CreateMap<AgregaHorasRequerimientoViewModel, HorasRequerimientoDTO>();
        }
    }
}