﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Servicios.Clientes.DTOs
{
    public class ClienteDTO
    {
        public int ClienteId { get; set; }
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string RucCliente { get; set; }
    }
}
