﻿using AutoMapper;
using GestionTicket.Repositorio.DDDContext;
using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.EF;
using GestionTicket.Repositorio.Impl;
using GestionTicket.Servicios.Clientes.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Servicios.Clientes
{
    public class ServicioCliente
    {
        private RepositorioCliente repositorioCliente;
        private IGestionTicketRepository _contexto;
        public ServicioCliente()
        {
            repositorioCliente = new RepositorioCliente(new GestionTicketDbContext("name=GestionTicket"));
            _contexto = new EFGestionTicketRepository();
        }
        public void Nuevo(ClienteDTO grabaClienteDto)
        {
            try
            {
                    //var cliente = Mapper.Map<ClienteDTO, Cliente>(grabaClienteDto);
                Cliente cliente = new Cliente(
                            grabaClienteDto.NombreCliente,
                            grabaClienteDto.Direccion,
                            grabaClienteDto.RucCliente
                            );
                   // repositorioCliente.Agregar(cliente);
                _contexto.ClienteRepository.Add(cliente);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                
                throw new Exception("Hubo un error : "+ex);
            }
            
        }

        public ClienteDTO TraerPorId(int id)
        {
            var cliente = _contexto
                .ClienteRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Cliente, ClienteDTO>(cliente);
        }

        public IList<ClienteDTO> Buscar(string busqueda)
        {
            var clientes = _contexto.ClienteRepository.GetAll(); //repositorioCliente.Traer();
            if(!string.IsNullOrEmpty(busqueda)) clientes=clientes
                .Where(c=>c.NombreCliente.Contains(busqueda)); //armando la consulta
            return Mapper.Map<IList<Cliente>, IList<ClienteDTO>>(clientes.ToList());
        }

        public IList<ClienteDTO> BuscarTodos()
        {
            var clientes = _contexto.ClienteRepository.GetAll();//repositorioCliente.Traer();
            return Mapper.Map<IList<Cliente>, IList<ClienteDTO>>(clientes.ToList());
        }
    }
}
