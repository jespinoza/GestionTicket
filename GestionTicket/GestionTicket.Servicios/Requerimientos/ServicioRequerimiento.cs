﻿using AutoMapper;
using GestionTicket.Repositorio.EF;
using GestionTicket.Servicios.Requerimientos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionTicket.Repositorio.Impl;
using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.DDDContext;
using GestionTicket.Servicios.Clientes.DTOs;
using GestionTicket.Servicios.Empleados.DTOs;

namespace GestionTicket.Servicios.Requerimientos
{
    public class ServicioRequerimiento
    {
        private RepositorioRequerimiento repositorioRequerimiento;
        private RepositorioCliente repositorioCliente;
        private RepositorioEmpleado repositorioEmpleado;
        private IGestionTicketRepository _contexto;
        public ServicioRequerimiento()
            {
                repositorioRequerimiento = new RepositorioRequerimiento(new GestionTicketDbContext("name=GestionTicket"));
                repositorioCliente = new RepositorioCliente(new GestionTicketDbContext("name=GestionTicket"));
                repositorioEmpleado = new RepositorioEmpleado(new GestionTicketDbContext("name=GestionTicket"));
                _contexto = new EFGestionTicketRepository();
            }
        public void Nuevo(RequerimientoDTO grabaRequerimientoDTO)
        {
            try 
            {
                    //var cliente = Mapper.Map<ClienteDTO, Cliente>(grabaClienteDto);
                Requerimiento requerimiento = new Requerimiento(
                    grabaRequerimientoDTO.ProyectoId,
                    grabaRequerimientoDTO.Tipo,
                    grabaRequerimientoDTO.Resumen,
                    grabaRequerimientoDTO.ClienteId,
                    grabaRequerimientoDTO.EmpleadoId,
                    grabaRequerimientoDTO.Entorno,
                    grabaRequerimientoDTO.FechaDeEntrega,
                    grabaRequerimientoDTO.FechaCierreRequerimiento,
                    grabaRequerimientoDTO.HorasEstimadas,
                    grabaRequerimientoDTO.Prioridad,
                    grabaRequerimientoDTO.Descripcion,
                    grabaRequerimientoDTO.Estado
                            );
                //repositorioRequerimiento.Agregar(requerimiento);
                _contexto.RequerimientoRepository.Add(requerimiento);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                
                throw new Exception("Hubo un error : "+ex);
            }
            
        }
        
        public void Actualizar(RequerimientoDTO requerimientoDTO)
        {
            var RequerimientoOld = _contexto.RequerimientoRepository.SingleOrDefault(x => x.Id == requerimientoDTO.RequerimientoId);

            RequerimientoOld.ProyectoId = requerimientoDTO.ProyectoId;
            RequerimientoOld.Tipo = requerimientoDTO.Tipo;
            RequerimientoOld.Resumen = requerimientoDTO.Resumen;
            RequerimientoOld.Entorno = requerimientoDTO.Entorno;
            RequerimientoOld.FechaDeEntrega = requerimientoDTO.FechaDeEntrega;
            RequerimientoOld.FechaCierreRequerimiento = requerimientoDTO.FechaCierreRequerimiento;
            RequerimientoOld.HorasEstimadas = requerimientoDTO.HorasEstimadas;
            RequerimientoOld.Descripcion = requerimientoDTO.Descripcion;
            RequerimientoOld.Estado = requerimientoDTO.Estado;

            _contexto.RequerimientoRepository.Update(RequerimientoOld);
            _contexto.Commit();
        }

        public void AgregarHorasDetalle(RequerimientoDetalleDTO requerimientoDetalleDTO)
        {
            try
            {
                RequerimientoDetalle requerimientoDetalle = new RequerimientoDetalle(
                    requerimientoDetalleDTO.RequeimientoId,
                    requerimientoDetalleDTO.EmpleadoId,
                    requerimientoDetalleDTO.HorasTrabajadas,
                    requerimientoDetalleDTO.FechaTrabajada,
                    requerimientoDetalleDTO.Descripcion
                            );
                //repositorioRequerimiento.Agregar(requerimiento);
                _contexto.RequerimientoDetalleRepository.Add(requerimientoDetalle);
                _contexto.Commit();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public void AgregarHoras()
        {
            try
            {
                //HorasPorRequerimiento horasrequerimiento = new HorasPorRequerimiento(
                //   grabaHorasRequerimiento.RequerimientoId,
                //   grabaHorasRequerimiento.EmpledoId,
                //   grabaHorasRequerimiento.FechaInicioReq,
                //   grabaHorasRequerimiento.HorasIngresadas,
                //   grabaHorasRequerimiento.Descripcion
                //);
                ////repositorioHorasPorRequerimiento.Agregar(horasrequerimiento);
                //_contexto.HorasPorRequerimientoRepository.Add(horasrequerimiento);
                //_contexto.Commit();
            }
            catch (Exception)
            {
                
                throw;
            }

            
        }

        public RequerimientoDTO TraerPorId(int id)
        {
            var requerimiento = _contexto
                .RequerimientoRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Requerimiento, RequerimientoDTO>(requerimiento);
        }

        public ClienteDTO TraerClientePorId(int id)
        {
            var cliente = _contexto
                .ClienteRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Cliente, ClienteDTO>(cliente);
        }

        public EmpleadoDTO TraerEmpleadoPorId(int id)
        {
            var empleado = _contexto
                .EmpleadoRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Empleado, EmpleadoDTO>(empleado);
        }
        public EmpleadoDTO TraerEmpleadoPorCorreo(string Correo)
        {
            var empleado = _contexto
                .EmpleadoRepository
                .SingleOrDefault(x => x.NombreUsuario == Correo);
            return Mapper.Map<Empleado, EmpleadoDTO>(empleado);
        }
        public IList<RequerimientoDTO> Buscar(string busqueda)
        {
            var puestos = _contexto.RequerimientoRepository.GetAll();//repositorioRequerimiento.Traer();
            if (!string.IsNullOrEmpty(busqueda)) puestos = puestos
                .Where(c=>c.Resumen.Contains(busqueda)); //armando la consulta
            return Mapper.Map<IList<Requerimiento>, IList<RequerimientoDTO>>(puestos.ToList());
        }

        public IList<RequerimientoDTO> BuscarTodos()
        {
            var requerimientos = _contexto.RequerimientoRepository.GetAll();//repositorioRequerimiento.Traer();
            return Mapper.Map<IList<Requerimiento>, IList<RequerimientoDTO>>(requerimientos.ToList());
        }

        public bool EliminarRequerimientoDetalle(int id)
        {
            bool valor = true;
            IList<RequerimientoDetalleDTO> ReqDetalle = BuscarDetalle(id);
            foreach (var item in ReqDetalle)
            {
                try
                {
                    var RequerimientoDetalleDelete = _contexto.RequerimientoDetalleRepository.SingleOrDefault(x => x.RequerimientoId == id);
                    _contexto.RequerimientoDetalleRepository.Delete(RequerimientoDetalleDelete);
                    _contexto.Commit();
                }
                catch (Exception)
                {
                    valor = false;
                    throw;
                }
            }
            return valor;
        }

        public bool EliminarRequerimiento(int id)
        {
            bool valor = true;
            try
            {
                var RequerimientoDelete = _contexto.RequerimientoRepository.SingleOrDefault(x => x.Id == id);
                _contexto.RequerimientoRepository.Delete(RequerimientoDelete);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                valor = false;
                throw;
            }
            return valor;
        }

        public IList<RequerimientoDetalleDTO> BuscarDetalle(int id)
        {
            var reqDetalle = _contexto.RequerimientoDetalleRepository.GetAll();
            reqDetalle = reqDetalle
                .Where(d => d.RequerimientoId == id);
            return Mapper.Map<IList<RequerimientoDetalle>, IList<RequerimientoDetalleDTO>>(reqDetalle.ToList());
        }
    }
}
