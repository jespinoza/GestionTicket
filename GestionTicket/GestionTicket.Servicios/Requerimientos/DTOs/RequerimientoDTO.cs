﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Servicios.Requerimientos.DTOs
{
    public class RequerimientoDTO
    {
        public int RequerimientoId { get; set; }
        public string ProyectoId { get; set; } 
        public string Tipo { get; set; }
        public string Resumen { get; set; }
        public int ClienteId { get; set; }
        public int EmpleadoId { get; set; }
        public string Entorno { get; set; }
        public DateTime FechaDeEntrega { get; set; }
        public DateTime FechaCierreRequerimiento { get; set; }
        public int HorasEstimadas { get; set; }
        public string Prioridad { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }

    public class RequerimientoDetalleDTO
    {
        public int RequeimientoId { get; set; }
        public int EmpleadoId { get; set; }
        public int HorasTrabajadas { get; set; }
        public DateTime FechaTrabajada { get; set; }
        public string Descripcion { get; set; }
    }
}
