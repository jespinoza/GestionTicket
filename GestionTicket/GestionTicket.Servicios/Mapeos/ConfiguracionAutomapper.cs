﻿using AutoMapper;
using GestionTicket.Servicios.Clientes.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionTicket.Repositorio.Dominio;
using GestionTicket.Servicios.AreaProyecto.DTOs;
using GestionTicket.Servicios.Puestos.DTOs;
using GestionTicket.Servicios.Empleados.DTOs;
using GestionTicket.Servicios.Requerimientos.DTOs;

namespace GestionTicket.Servicios.Mapeos
{
    public class ServiciosConfiguracionAutomapper
    {
        public static void Configuracion()
        {
            Area();
            Puesto();
            Empleado();
            Cliente();
            Requerimiento();
        }
        private static void Area()
        {
            Mapper.CreateMap<AreaDTO, Area>();
            Mapper.CreateMap<Area, AreaDTO>()
                .ForMember(dest => dest.AreaId, map => map.MapFrom(orig => orig.Id));
        }
        private static void Puesto()
        {
            Mapper.CreateMap<PuestoDTO, Puesto>();
            Mapper.CreateMap<Puesto, PuestoDTO>()
                .ForMember(dest => dest.PuestoId, map => map.MapFrom(orig => orig.Id));
        }
        private static void Empleado()
        {
            Mapper.CreateMap<Empleado, EmpleadoDTO>()
                .ForMember(dest => dest.EmpleadoId, map => map.MapFrom(orig => orig.Id));
            Mapper.CreateMap<GrabaEmpleadoDTO, Empleado>()
                .ForMember(dest => dest.NombreUsuario, map => map.MapFrom(orig => orig.CorreoElectronico))
                .ForMember(dest => dest.Url, map => map.UseValue(string.Empty)); ;
        }
        private static void Cliente()
        {
            Mapper.CreateMap<ClienteDTO, Cliente>();
            Mapper.CreateMap<Cliente, ClienteDTO>()
                .ForMember(dest => dest.ClienteId, map => map.MapFrom(orig => orig.Id));
        }
        private static void Requerimiento()
        {
            Mapper.CreateMap<RequerimientoDTO, Requerimiento>();
            Mapper.CreateMap<RequerimientoDetalleDTO,RequerimientoDetalle>();
            Mapper.CreateMap<Requerimiento, RequerimientoDTO>()
                .ForMember(dest => dest.RequerimientoId, map => map.MapFrom(orig => orig.Id));
            Mapper.CreateMap<RequerimientoDetalle, RequerimientoDetalleDTO>()
                .ForMember(dest=>dest.RequeimientoId,map=>map.MapFrom(orig=>orig.RequerimientoId))
                .ForMember(dest=>dest.Descripcion,map=>map.MapFrom(orig=>orig.DetalleTrabajo));
            //Mapper.CreateMap<HorasRequerimientoDTO, HorasPorRequerimiento>();
        }

    }
}
