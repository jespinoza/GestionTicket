﻿using GestionTicket.Repositorio.EF;
using GestionTicket.Servicios.Empleados.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionTicket.Repositorio.Impl;
using GestionTicket.Repositorio.Dominio;
using AutoMapper;
using GestionTicket.Repositorio.DDDContext;
using System.Threading.Tasks;

namespace GestionTicket.Servicios.Empleados
{
    public class ServicioEmpleado
    {
        private RepositorioEmpleado repositorioEmpleado;
        private IGestionTicketRepository _contexto;
         public ServicioEmpleado()
            {
                repositorioEmpleado = new RepositorioEmpleado(new GestionTicketDbContext("name=GestionTicket"));
                _contexto = new EFGestionTicketRepository();
            }
        public void Nuevo(GrabaEmpleadoDTO grabaEmpleadoDTO)
        {
            try
            {
                //var empleado = Mapper.Map<GrabaEmpleadoDTO, Empleado>(grabaEmpleadoDTO);
                //var cliente = Mapper.Map<ClienteDTO, Cliente>(grabaClienteDto);
                Empleado empleado = new Empleado(
                            grabaEmpleadoDTO.Nombres,
                            grabaEmpleadoDTO.ApellidoPaterno,
                            grabaEmpleadoDTO.ApellidoMaterno,
                            grabaEmpleadoDTO.PuestoId,
                            grabaEmpleadoDTO.AreaId,
                            grabaEmpleadoDTO.FechaInicioLabores,
                            grabaEmpleadoDTO.NombreUsuario,
                            string.Empty
                            );

                _contexto.EmpleadoRepository.Add(empleado);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                
                throw new Exception("Hubo un error : "+ex);
            }
            
        }

        public EmpleadoDTO TraerPorId(int id)
        {
            var empleado = _contexto
                .EmpleadoRepository
                .SingleOrDefault(x => x.Id == id);
            return Mapper.Map<Empleado, EmpleadoDTO>(empleado);
        }

        public IList<EmpleadoDTO> Buscar(string busqueda)
        {
            var empleados = _contexto.EmpleadoRepository.GetAll();//repositorioEmpleado.Traer();
            if (!string.IsNullOrEmpty(busqueda)) empleados = empleados
                .Where(c=>c.Nombres.Contains(busqueda)); //armando la consulta
            return Mapper.Map<IList<Empleado>, IList<EmpleadoDTO>>(empleados.ToList());
        }

        public IList<EmpleadoDTO> BuscarTodos()
        {
            var puestos = _contexto.EmpleadoRepository.GetAll();//repositorioEmpleado.Traer();
            return Mapper.Map<IList<Empleado>, IList<EmpleadoDTO>>(puestos.ToList());
        }

        public string ObtenerRutaPerfil(string nombreUsuario)
        {
            try
            {
                var empleado = _contexto.EmpleadoRepository.SingleOrDefault(x => x.NombreUsuario == nombreUsuario);
                return (string.IsNullOrEmpty(empleado.Url) ? empleado.Id.ToString() : empleado.Url);
            }
            catch (Exception e)
            {
                
                return string.Empty;
            }            
        }
    }
}
