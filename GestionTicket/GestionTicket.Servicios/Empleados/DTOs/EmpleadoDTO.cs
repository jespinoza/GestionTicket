﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Servicios.Empleados.DTOs
{
    public class EmpleadoDTO
    {
        public int EmpleadoId { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int PuestoId { get; set; }
        public int AreaId { get; set; }
        public string CorreoElectronico { get; set; } 
        public DateTime FechaInicioLabores { get; set; }
        public string NombreUsuario { get; set; }
        public string Url { get; set; }
    }
}
