﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Servicios.AreaProyecto.DTOs
{
    public class AreaDTO
    {
        public int AreaId { get; set; }
        public string NombreArea { get; set; }
        public string DescripcionArea { get; set; }
    }
}
