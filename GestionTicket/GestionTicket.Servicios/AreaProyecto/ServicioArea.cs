﻿using AutoMapper;
using GestionTicket.Repositorio.EF;
using GestionTicket.Servicios.AreaProyecto.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.Impl;
using GestionTicket.Repositorio.DDDContext;

namespace GestionTicket.Servicios.AreaProyecto
{
    public class ServicioArea
    {
         private RepositorioArea repositorioArea;
         private IGestionTicketRepository _contexto;
         public ServicioArea()
            {
                repositorioArea = new RepositorioArea(new GestionTicketDbContext("name=GestionTicket"));
                _contexto = new EFGestionTicketRepository();
            }
        public void Nuevo(AreaDTO grabaAreaDTO)
        {
            try
            {
                    //var cliente = Mapper.Map<ClienteDTO, Cliente>(grabaClienteDto);
                Area area = new Area(
                            grabaAreaDTO.NombreArea,
                            grabaAreaDTO.DescripcionArea
                            );
                //repositorioArea.Agregar(cliente);
                _contexto.AreaRepository.Add(area);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                
                throw new Exception("Hubo un error : "+ex);
            }
            
        }

        public IList<AreaDTO> Buscar(string busqueda)
        {
            var areas = _contexto.AreaRepository.GetAll();//repositorioArea.Traer();
            if (!string.IsNullOrEmpty(busqueda)) areas = areas
                .Where(c=>c.NombreArea.Contains(busqueda)); //armando la consulta
            return Mapper.Map<IList<Area>, IList<AreaDTO>>(areas.ToList());
        }

        public IList<AreaDTO> BuscarTodos()
        {
            var areas = _contexto.AreaRepository.GetAll();//repositorioArea.Traer();
           return Mapper.Map<IList<Area>, IList<AreaDTO>>(areas.ToList());
        }
    }
}
