﻿using AutoMapper;
using GestionTicket.Repositorio.EF;
using GestionTicket.Servicios.Puestos.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.Impl;
using GestionTicket.Repositorio.DDDContext;

namespace GestionTicket.Servicios.Puestos
{
    public class ServicioPuesto
    {
        private RepositorioPuesto repositorioPuesto;
        private IGestionTicketRepository _contexto;
        public ServicioPuesto()
            {
                repositorioPuesto = new RepositorioPuesto(new GestionTicketDbContext("name=GestionTicket"));
                _contexto = new EFGestionTicketRepository();
            }
        public void Nuevo(PuestoDTO grabaPuestoDTO)
        {
            try
            {
                    //var cliente = Mapper.Map<ClienteDTO, Cliente>(grabaClienteDto);
                Puesto puesto = new Puesto(
                            grabaPuestoDTO.NombrePuesto,
                            grabaPuestoDTO.DescripcionPuesto
                            );
                //repositorioPuesto.Agregar(puesto);
                _contexto.PuestoRepository.Add(puesto);
                _contexto.Commit();
            }
            catch (Exception ex)
            {
                
                throw new Exception("Hubo un error : "+ex);
            }
            
        }

        public IList<PuestoDTO> Buscar(string busqueda)
        {
            var puestos = _contexto.PuestoRepository.GetAll();//repositorioPuesto.Traer();
            if (!string.IsNullOrEmpty(busqueda)) puestos = puestos
                .Where(c=>c.NombrePuesto.Contains(busqueda)); //armando la consulta
            return Mapper.Map<IList<Puesto>, IList<PuestoDTO>>(puestos.ToList());
        }

        public IList<PuestoDTO> BuscarTodos()
        {
            var puestos = _contexto.PuestoRepository.GetAll();//repositorioPuesto.Traer();
            return Mapper.Map<IList<Puesto>, IList<PuestoDTO>>(puestos.ToList());
        }
    }
}
