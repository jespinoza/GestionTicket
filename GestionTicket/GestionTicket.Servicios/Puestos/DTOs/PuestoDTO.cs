﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Servicios.Puestos.DTOs
{
    public class PuestoDTO
    {
        public int PuestoId { get; set; }
        public string NombrePuesto { get; set; }
        public string DescripcionPuesto { get; set; }
    }
}
