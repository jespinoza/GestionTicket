﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Dominio
{
    public class Empleado :Entidad
    {
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public int PuestoId { get; set; }
        public int AreaId { get; set; }
        public DateTime FechaInicioLabores { get; set; }
        public string NombreUsuario { get; set; }
        public string Url { get; set; }

        public Empleado(string nombres, string apellidoPaterno, string apellidoMaterno, int puestoId, int AreadId, DateTime fechaInicioLabores,string nombreUsuario,string url) 
        {
            this.Nombres = nombres;
            this.ApellidoPaterno = apellidoPaterno;
            this.ApellidoMaterno = apellidoMaterno;
            this.PuestoId = puestoId;
            this.AreaId = AreadId;
            this.FechaInicioLabores = fechaInicioLabores;
            this.NombreUsuario = nombreUsuario;
            this.Url = url;
        }
        public Empleado() { }
    }
}
