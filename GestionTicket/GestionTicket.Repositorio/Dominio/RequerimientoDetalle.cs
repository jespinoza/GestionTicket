﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.Dominio
{
    public class RequerimientoDetalle : Entidad
    {
        public int RequerimientoId { get; set; }
        public int EmpleadoId { get; set; }
        public int HorasTrabajadas { get; set; }
        public DateTime FechaTrabajada { get; set; }
        public string DetalleTrabajo { get; set; }

         public RequerimientoDetalle(int requerimientoId,int empleadoId,int horasTrabajadas,DateTime fechaTrabajada,string detalleTrabajo)
         {
             this.RequerimientoId = requerimientoId;
             this.EmpleadoId = empleadoId;
             this.HorasTrabajadas = horasTrabajadas;
             this.FechaTrabajada = fechaTrabajada;
             this.DetalleTrabajo = detalleTrabajo;
         }

         public RequerimientoDetalle()
         { }
    } 
   
}
