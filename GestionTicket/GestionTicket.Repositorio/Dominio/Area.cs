﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Dominio
{
    public class Area :Entidad
    {
        public string NombreArea { get; set; }
        public string DescripcionArea { get; set; }

        public Area(string nombreArea, string descripcionArea)
        {
            this.NombreArea = nombreArea;
            this.DescripcionArea = descripcionArea;
        }
        public Area() { }
    }
}
