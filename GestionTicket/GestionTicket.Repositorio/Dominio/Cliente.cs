﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Dominio
{
    public class Cliente : Entidad
    {
        public string NombreCliente { get; set; }
        public string Direccion { get; set; }
        public string RucCliente { get; set; }

        public Cliente(string nombreCliente, string direccion, string rucCliente)
        {
            this.NombreCliente = nombreCliente;
            this.Direccion = direccion;
            this.RucCliente = rucCliente;
        }
        public Cliente() { }
    }
}
