﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Dominio
{
    public class Puesto : Entidad
    {
        public string NombrePuesto { get; set; }
        public string DescripcionPuesto { get; set; }

        public Puesto(string nombrePuesto, string descripcionPuesto)
        {
            this.NombrePuesto = nombrePuesto;
            this.DescripcionPuesto = descripcionPuesto;
        }
        public Puesto() { }
    }
}
