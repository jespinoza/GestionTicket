﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Dominio
{
    public class Requerimiento : Entidad
    {
        public string ProyectoId { get; set; }
        public string Tipo { get; set; }
        public string Resumen { get; set; }
        public int ClienteId { get; set; }
        public int EmpleadoId { get; set; }
        public string Entorno { get; set; }
        public DateTime FechaDeEntrega { get; set; }
        public DateTime FechaCierreRequerimiento { get; set; }
        public int HorasEstimadas { get; set; }
        public string Prioridad { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

        public Requerimiento(string proyectoId, string tipo, string resumen, int clienteId,
                             int empleadoId, string entorno, DateTime fechaDeEntrega,
                             DateTime fechaCierreRequerimiento, int horasEstimadas, 
                             string prioridad, string descripcion, string estado)
        {
            this.ClienteId = clienteId;
            this.Descripcion = descripcion;
            this.EmpleadoId = empleadoId;
            this.Entorno = entorno;
            this.FechaDeEntrega = fechaDeEntrega;
            this.FechaCierreRequerimiento = fechaCierreRequerimiento;
            this.HorasEstimadas = horasEstimadas;
            this.Prioridad = prioridad;
            this.ProyectoId = proyectoId;
            this.Resumen = resumen;
            this.Tipo = tipo;
            this.Estado = estado;
        }

        public Requerimiento() { }
    }
}
