﻿using GestionTicket.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GestionTicket.Repositorio.Dominio;

namespace GestionTicket.Repositorio.Impl
{
    public class RepositorioEmpleado : RepositorioBase<Empleado, GestionTicketDbContext>
    {
        public RepositorioEmpleado(GestionTicketDbContext context)
            : base(context)
        {

        }
    }
}
