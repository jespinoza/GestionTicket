﻿using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Impl
{
    public class RepositorioArea : RepositorioBase<Area, GestionTicketDbContext>
    {
        public RepositorioArea(GestionTicketDbContext context)
            : base(context)
        {

        }
    }
}
