﻿using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Impl
{
    public class RepositorioPuesto : RepositorioBase<Puesto, GestionTicketDbContext>
    {
        public RepositorioPuesto(GestionTicketDbContext context)
            : base(context)
        {

        }
    }
}
