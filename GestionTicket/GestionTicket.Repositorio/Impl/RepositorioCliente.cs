﻿using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.EF;

namespace GestionTicket.Repositorio.Impl
{
    public class RepositorioCliente : RepositorioBase<Cliente, GestionTicketDbContext>
    {
        public RepositorioCliente(GestionTicketDbContext context)
            : base(context)
        {

        }
    }
}
