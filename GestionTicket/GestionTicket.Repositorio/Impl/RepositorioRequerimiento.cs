﻿using GestionTicket.Repositorio.Dominio;
using GestionTicket.Repositorio.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GestionTicket.Repositorio.Impl
{
    public class RepositorioRequerimiento: RepositorioBase<Requerimiento, GestionTicketDbContext>
    {
        public RepositorioRequerimiento(GestionTicketDbContext context)
            : base(context)
        {

        }
    }
}
