namespace GestionTicket.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NombreUsuarioUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empleadoes", "NombreUsuario", c => c.String());
            AddColumn("dbo.Empleadoes", "Url", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empleadoes", "Url");
            DropColumn("dbo.Empleadoes", "NombreUsuario");
        }
    }
}
