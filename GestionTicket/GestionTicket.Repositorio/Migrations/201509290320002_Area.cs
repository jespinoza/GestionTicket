namespace GestionTicket.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Area : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreArea = c.String(),
                        DescripcionArea = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Areas");
        }
    }
}
