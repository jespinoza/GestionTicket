namespace GestionTicket.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class puesto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Puestoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombrePuesto = c.String(),
                        DescripcionPuesto = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Puestoes");
        }
    }
}
