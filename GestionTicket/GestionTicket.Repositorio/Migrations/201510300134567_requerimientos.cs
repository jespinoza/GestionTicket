namespace GestionTicket.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requerimientos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Requerimientoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProyectoId = c.String(),
                        Tipo = c.String(),
                        Resumen = c.String(),
                        ClienteId = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        Entorno = c.String(),
                        FechaDeEntrega = c.DateTime(nullable: false),
                        FechaCierreRequerimiento = c.DateTime(),
                        HorasEstimadas = c.Int(nullable: false),
                        Prioridad = c.String(),
                        Descripcion = c.String(),
                        Estado = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RequerimientoDetalles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequerimientoId = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        HorasTrabajadas = c.Int(nullable: false),
                        FechaTrabajada = c.DateTime(nullable: false),
                        DetalleTrabajo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RequerimientoDetalles");
            DropTable("dbo.Requerimientoes");
        }
    }
}
