﻿using GestionTicket.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.DDDContext
{
    public class PuestoRepositorio : GenericRepository<EFGestionTicketRepository,Puesto>
    {
        public PuestoRepositorio(EFGestionTicketRepository context)
            : base(context)
        { }
    }
}
