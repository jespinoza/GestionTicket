﻿using GestionTicket.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.DDDContext
{
    public class RequerimientoRepositorio:GenericRepository<EFGestionTicketRepository,Requerimiento>
    {
        public RequerimientoRepositorio(EFGestionTicketRepository context)
            : base(context)
        { }
    }
}
