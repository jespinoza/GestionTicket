﻿using GestionTicket.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.DDDContext
{
    public class EFGestionTicketRepository : DbContext, IGestionTicketRepository 
    {
        private readonly AreaRepositorio _areaRepo;
        private readonly ClienteRepositorio _clienteRepo;
        private readonly EmpleadoRepositorio _empleadoRepo;
        private readonly PuestoRepositorio _puestoRepo;
        private readonly RequerimientoRepositorio _requerimientoRepo;
        private readonly RequerimientoDetalleRepositorio _requerimientoDetalleRepo;

        public DbSet<Area> Areas { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<Requerimiento> Requerimientos { get; set; }
        public DbSet<RequerimientoDetalle> RequerimientoDetalles { get; set; }

        public EFGestionTicketRepository()
            : base("name=GestionTicket")
        {
            _areaRepo = new AreaRepositorio(this);
            _clienteRepo = new ClienteRepositorio(this);
            _empleadoRepo = new EmpleadoRepositorio(this);
            _puestoRepo = new PuestoRepositorio(this);
            _requerimientoRepo = new RequerimientoRepositorio(this);
            _requerimientoDetalleRepo = new RequerimientoDetalleRepositorio(this);
        }

        #region IUnitOfWork Implementation



        public void Commit()
        {
            this.SaveChanges();
        }

        #endregion

        public IGenericRepository<Area> AreaRepository
        {
            get { return _areaRepo; }
        }

        public IGenericRepository<Cliente> ClienteRepository
        {
            get { return _clienteRepo; }
        }

        public IGenericRepository<Empleado> EmpleadoRepository
        {
            get { return _empleadoRepo; }
        }

        public IGenericRepository<Puesto> PuestoRepository
        {
            get { return _puestoRepo; }
        }

        public IGenericRepository<Requerimiento> RequerimientoRepository
        {
            get { return _requerimientoRepo; }
        }

        public IGenericRepository<RequerimientoDetalle> RequerimientoDetalleRepository
        {
            get { return _requerimientoDetalleRepo; }
        }
    }
}
