﻿using GestionTicket.Repositorio.Dominio;

namespace GestionTicket.Repositorio.DDDContext
{
    public class ClienteRepositorio : GenericRepository<EFGestionTicketRepository,Cliente>
    {
        public ClienteRepositorio(EFGestionTicketRepository context)
            :base(context)
        { 
        }
    }
}
