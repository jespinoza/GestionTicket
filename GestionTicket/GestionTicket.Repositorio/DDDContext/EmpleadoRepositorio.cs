﻿using GestionTicket.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.DDDContext
{
    public class EmpleadoRepositorio : GenericRepository<EFGestionTicketRepository,Empleado>
    {
        public EmpleadoRepositorio(EFGestionTicketRepository context)
            : base(context)
        { }
    }
}
