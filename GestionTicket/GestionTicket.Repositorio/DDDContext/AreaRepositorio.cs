﻿using GestionTicket.Repositorio.Dominio;

namespace GestionTicket.Repositorio.DDDContext
{
    public class AreaRepositorio : GenericRepository<EFGestionTicketRepository, Area>
    {
        public AreaRepositorio(EFGestionTicketRepository context)
            : base(context)
        {
        }
    }
}
