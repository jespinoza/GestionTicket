﻿using GestionTicket.Repositorio.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTicket.Repositorio.DDDContext
{
    public interface IGestionTicketRepository : IDisposable
    {
        IGenericRepository<Area> AreaRepository { get; }
        IGenericRepository<Cliente> ClienteRepository { get; }
        IGenericRepository<Empleado> EmpleadoRepository { get; }
        IGenericRepository<Puesto> PuestoRepository { get; }
        IGenericRepository<Requerimiento> RequerimientoRepository { get; }
        IGenericRepository<RequerimientoDetalle> RequerimientoDetalleRepository { get; }
        void Commit();
    }
}
