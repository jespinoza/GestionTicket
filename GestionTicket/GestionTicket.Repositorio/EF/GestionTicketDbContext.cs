﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionTicket.Repositorio.Dominio;

namespace GestionTicket.Repositorio.EF
{
   public class GestionTicketDbContext : DbContext
    {
       public GestionTicketDbContext()
           : base("name=GestionTicket")
        {
                
        }

       public GestionTicketDbContext(string conexion)
           : base(conexion)
        {

        }

       public virtual DbSet<Cliente> Cliente { get; set; }
       public virtual DbSet<Area> Area { get;set; }
       public virtual DbSet<Puesto> Puesto { get; set; }
       public virtual DbSet<Empleado> Empleado { get; set; }
       public virtual DbSet<Requerimiento> Requerimiento { get; set; }
       public virtual DbSet<RequerimientoDetalle> RequerimientoDetalle { get; set; }
    }
}
